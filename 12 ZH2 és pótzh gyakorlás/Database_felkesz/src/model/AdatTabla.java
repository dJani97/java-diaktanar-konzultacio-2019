package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdatTabla {
    private List<Rekord> rekordok = new ArrayList<>();

    public List<Rekord> osszesRekord() {
        // fontos, hogy itt olyan rekord listát adjunk vissza, melyhez később nem lehet hozzányúlni
        return Collections.unmodifiableList(this.rekordok);
    }

    public Rekord rekordIdAlapjan(int rekordID) throws Exception {
        for (Rekord rekord : this.rekordok) {
            if(rekord.getId() == rekordID) {
                return rekord;
            }
        }
        throw new Exception("A megadott ID-vel rendelkező rekord nem található!");
    }

    public void rekordBeszurasa(Rekord rekord) throws Exception {
        if (!this.rekordok.contains(rekord)) {
            this.rekordok.add(rekord);
        } else {
            throw new Exception("A beszúrni kívánt rekord már létezik!");
        }
    }

    public void rekordModositasa(Rekord rekord) throws Exception {
        this.rekordTorlese(rekord.getId());
        this.rekordBeszurasa(rekord);
    }

    public void rekordTorlese(int rekordID) throws Exception {
        Rekord rekord = this.rekordIdAlapjan(rekordID);
        this.rekordok.remove(rekord);
    }
}
