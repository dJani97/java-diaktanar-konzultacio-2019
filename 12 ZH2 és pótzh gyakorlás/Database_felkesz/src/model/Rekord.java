package model;

import java.time.LocalDate;
import java.util.Objects;

public class Rekord {

    /**
     *  Feladatban felsorolt mezők:
     */

    private int id;
    private String nev;
    private LocalDate szuletesiIdo;
    private String szuletesiHely;
    private String lakcim;

    /**
     *  Ez az osztály innentől kezdve kizárólag az ALT + Insert gombok lenyomásával
     *  generálható kódot tartalmaz, kézzel nem lett módosítva!
     */

    public Rekord(int id, String nev, LocalDate szuletesiIdo, String szuletesiHely, String lakcim) {
        this.id = id;
        this.nev = nev;
        this.szuletesiIdo = szuletesiIdo;
        this.szuletesiHely = szuletesiHely;
        this.lakcim = lakcim;
    }

    @Override
    public String toString() {
        return  " " + id +
                ",\t" + nev +
                ",\t" + szuletesiIdo +
                ",\t" + szuletesiHely +
                ",\t" + lakcim;
    }

    /**
     * Az alábbi két metódus az Alt+Insert menüben az "Equals() and hashCode()" opcióval generálható,
     * úgy, hogy csak az id mezőt választjuk ki, mint összehasonlítási alap.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rekord rekord = (Rekord) o;
        return id == rekord.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }



    /**
     * Szokásos get-set metódusok:
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public LocalDate getSzuletesiIdo() {
        return szuletesiIdo;
    }

    public void setSzuletesiIdo(LocalDate szuletesiIdo) {
        this.szuletesiIdo = szuletesiIdo;
    }

    public String getSzuletesiHely() {
        return szuletesiHely;
    }

    public void setSzuletesiHely(String szuletesiHely) {
        this.szuletesiHely = szuletesiHely;
    }

    public String getLakcim() {
        return lakcim;
    }

    public void setLakcim(String lakcim) {
        this.lakcim = lakcim;
    }
}
