package model;

import java.time.LocalDate;

public class RekordFactory {
    //static int jelenlegiID = 0;

    public static Rekord rekordLetrehozasa(int id, String nev, LocalDate szuletesiIdo, String szuletesiHely, String lakcim) {
        //int rekordID = jelenlegiID;
        //jelenlegiID++;
        return new Rekord(id, nev, szuletesiIdo, szuletesiHely, lakcim);
    }
}
