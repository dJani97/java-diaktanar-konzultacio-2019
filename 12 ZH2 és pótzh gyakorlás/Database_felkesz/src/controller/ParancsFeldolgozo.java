package controller;

import model.Rekord;
import model.RekordFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ParancsFeldolgozo {

    // adatbázis parancsai:

    public final String LISTAZAS = "LISTAZ";
    public final String HOZZAADAS = "HOZZAAD";
    public final String MODOSITAS = "MODOSIT";
    public final String TORELS = "TOROL";
    public final String MENTES = "BACKUP";
    public final String SEGITSEG = "HELP";
    public final String KILEPES = "KILEP";
    public final String PARANCS_FORMATUM = "<Id,Név,2000-01-01,SzületésiHely,Lakcím>";
    public final String ID_FORMATUM = "<ID>";

    // feldolgozó parancsai:
    public final String ONLINE = "ONLINE";
    public final String OFFLINE = "OFFLINE";
    public final String BUFFER_TOROL = "BUFFER_TOROL";
    public final String BUFFER_LISTAZ = "BUFFER_LISTAZ";

    private MainConsoleController mainConsoleController;
    private boolean online = true;
    private List<String> parancsBuffer = new ArrayList<>();


    public ParancsFeldolgozo(MainConsoleController mainConsoleController) {
        this.mainConsoleController = mainConsoleController;
    }

    public void feldolgoz(String parancs) {

        String parancsElsoSzava = parancs.split(" ")[0];

        switch (parancsElsoSzava) {
            case ONLINE:
                this.online = true;
                break;
            case OFFLINE:
                this.online = false;
                break;
            case MENTES:
                mainConsoleController.adatbazisMentese();
                break;
            case BUFFER_TOROL:
                String idString = parancs.split(" ", 2)[1];
                int parancsId = this.idFeldolgozasa(idString);
                this.parancsBuffer.remove(parancsId);
                break;
            case BUFFER_LISTAZ:
                this.mainConsoleController.bufferListazasa(this.parancsBuffer);
                break;
            default:
                parancsBuffer.add(parancs);
        }


        if (online) {
            for (String jelenlegiParancs : parancsBuffer) {
                vegrehajt(jelenlegiParancs);
            }
            parancsBuffer.clear();
        }
    }

    private void vegrehajt(String parancs) {
        if (parancs.contains(LISTAZAS)) {
            this.mainConsoleController.rekordokListazasa();
        }
        else if (parancs.contains(HOZZAADAS)) {
            String rekordString = parancs.split(" ", 2)[1];
            Rekord r = this.rekordFeldolgozasa(rekordString);
            this.mainConsoleController.ujRekordLetrehozasa(r);
        }
        else if (parancs.contains(MODOSITAS)) {
            String rekordString = parancs.split(" ", 2)[1];
            Rekord rekord = this.rekordFeldolgozasa(rekordString);
            this.mainConsoleController.rekordModositasa(rekord);
        }
        else if (parancs.contains(TORELS)) {
            String idString = parancs.split(" ", 2)[1];
            int id = this.idFeldolgozasa(idString);
            this.mainConsoleController.rekordTorlese(id);
        }
        else if (parancs.contains(SEGITSEG)) {
            this.mainConsoleController.parancsokListazasa();
        }
        else if (parancs.contains(KILEPES)) {
            this.mainConsoleController.kilepesAProgrambol();
        }
        else {
            this.mainConsoleController.hibasParancs();
        }
    }

    /*
     * Paraméterek leválasztása:
     */
    private Rekord rekordFeldolgozasa(String rekordString) {
        Rekord rekord;
        String[] rekordAdatai = rekordString.split(",");

        int id = Integer.parseInt(rekordAdatai[0]);
        String nev = rekordAdatai[1];
        LocalDate szuletesiIdo = LocalDate.parse(rekordAdatai[2]);
        String szuletesiHely = rekordAdatai[3];
        String lakcim = rekordAdatai[4];

        rekord = RekordFactory.rekordLetrehozasa(id, nev, szuletesiIdo, szuletesiHely, lakcim);
        return rekord;
    }

    private int idFeldolgozasa(String idString) {
        return Integer.parseInt(idString);
    }

}
