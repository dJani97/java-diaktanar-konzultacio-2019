package controller;

import model.AdatTabla;
import model.Rekord;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class MainConsoleController {
    private AdatTabla adatTabla;
    private ParancsFeldolgozo parancsFeldolgozo;
    private boolean fut;
    private Scanner scanner = new Scanner(System.in);

    public void start() {
        inicializalas();
        varakozasParancsokra();


    }

    private void varakozasParancsokra() {
        while (fut) {
            try {
                System.out.print("$> ");
                String parancs = this.scanner.nextLine();
                this.parancsFeldolgozo.feldolgoz(parancs);
                System.out.println("Parancs végrehajtva!");

            } catch (Exception ex) {
                System.out.println("Hiba: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    private void inicializalas() {
        fut = true;

        // adatbázis létrehozása
        adatTabla = new AdatTabla();

        // parancsFeldolgozo értelmező létrehozása
        parancsFeldolgozo = new ParancsFeldolgozo(this);

        // néhány minta rekord beszúrása
        kezdoRekordokBeszurasa();

        System.out.println("Üdvözlöm az adatbázis-kezelő alkalmazásban!");
        parancsokListazasa();
    }

    private void kezdoRekordokBeszurasa() {
        this.parancsFeldolgozo.feldolgoz("HOZZAADAS 1,Bella,1995-01-01,Pecs,Siklos");
        this.parancsFeldolgozo.feldolgoz("HOZZAADAS 2,Béla,1996-01-01,Pecs,Pecs");
        this.parancsFeldolgozo.feldolgoz("HOZZAADAS 3,Tibor,1997-01-01,Budapest,Humbákfalva");
    }

    public void parancsokListazasa() {
        System.out.println("Kérem használja a következő parancsok egyikét:");
        System.out.println(this.parancsFeldolgozo.LISTAZAS);
        System.out.println(this.parancsFeldolgozo.HOZZAADAS + " " + this.parancsFeldolgozo.PARANCS_FORMATUM);
        System.out.println(this.parancsFeldolgozo.MODOSITAS + " " + this.parancsFeldolgozo.PARANCS_FORMATUM);
        System.out.println(this.parancsFeldolgozo.TORELS + " " + this.parancsFeldolgozo.ID_FORMATUM);
        System.out.println(this.parancsFeldolgozo.MENTES);
        System.out.println(this.parancsFeldolgozo.SEGITSEG);

        System.out.println("\nParancsok menedzselése:");
        System.out.println(this.parancsFeldolgozo.ONLINE);
        System.out.println(this.parancsFeldolgozo.OFFLINE);
        System.out.println(this.parancsFeldolgozo.BUFFER_LISTAZ);
        System.out.println(this.parancsFeldolgozo.BUFFER_TOROL);
        System.out.println();
    }

    public void hibasParancs() {
        System.out.println("Hibás parancs, vagy paraméterek. Használd a "
                + this.parancsFeldolgozo.SEGITSEG + " parancsot példákért.");
    }

    public void rekordokListazasa() {
        List<Rekord> rekordok = this.adatTabla.osszesRekord();
        for (Rekord rekord : rekordok) {
            System.out.println(rekord);
        }
    }

    public void ujRekordLetrehozasa(Rekord rekord) {
        try {
            this.adatTabla.rekordBeszurasa(rekord);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void rekordModositasa(Rekord rekord) {
        try {
            this.adatTabla.rekordModositasa(rekord);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void rekordTorlese(int id) {
        try {
            this.adatTabla.rekordTorlese(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void kilepesAProgrambol() {
        this.fut = false;
    }

    public void bufferListazasa(List<String> buffer) {
        int i = 0;
        for (String s : buffer) {
            System.out.println(i + ". " + s);
            i++;
        }
    }

    public void adatbazisMentese() {
        try {
            FileWriter fileWriter = new FileWriter("src/adatok/uj_adatfajl.txt");

            for (Rekord rekord: this.adatTabla.osszesRekord()) {

                String kiirando = rekord.getId() + ", " + rekord.getNev();
                fileWriter.write(kiirando);

                fileWriter.write("\r\n");
            }

            fileWriter.flush();
            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Nem létező útvonal, vagy nincs jogosultság");
            e.printStackTrace();
        }
    }
}
