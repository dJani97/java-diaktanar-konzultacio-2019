import controller.MainConsoleController;

public class Main {

    public static void main(String[] args) {
        new MainConsoleController().start();
    }

}
