package com.company.controller;

import com.company.Translator;
import com.company.model.Flat;
import com.company.model.FlatStorage;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
   Flat Controller class where all the interactions with the user are performed.
 */
public class FlatController {

    private Scanner scanner = new Scanner(System.in);
    private FlatStorage storage = new FlatStorage();
    private Translator translator = Translator.getSharedInstance();

    /**
     * Starting point of the program
     */
    public void start() {
        chooseLanguage();
        displayMenu();
    }

    /**
     * Method where user chooses preferred language from the existing ones
     */
    private void chooseLanguage() {
        boolean correctInput = false;

        // Ask for an input until user input is correct
        do {
            System.out.println("Please choose preferred language: 1 - English");

            try {
                int userInput = scanner.nextInt();
                translator.setLanguage(userInput);
                correctInput = true;
            } catch (Exception e) {
                System.out.println("Please choose a digit option from the list!");
                e.printStackTrace();
            }

        } while (!correctInput);
    }

    /**
     * Method that displays the main menu with several actions
     */
    private void displayMenu() {
        boolean isRunning = true;
        do {
            try {
                System.out.println(translator.translate("menu"));
                Scanner scanner = new Scanner(System.in);
                int answer = scanner.nextInt();
                switch (answer) {
                    case 1:
                        registerFlat();
                        break;
                    case 2:
                        findAndDeleteFlat();
                        break;
                    case 3:
                        listFlats();
                        break;
                    case 0:
                        isRunning = false;
                    default:
                        System.out.println("Please choose one action form the menu.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Please check that your input is correct!");
            }

        } while (isRunning);

    }

    private void registerFlat() {
        System.out.print(translator.translate("registration.address"));
        String address = scanner.nextLine();

        /*
         Skip the next line.
         Necessary because otherwise the scanner can
         continue scanning without user input.
        */
        scanner.nextLine();

        System.out.print(translator.translate("registration.area"));
        String area = scanner.nextLine();

        System.out.print(translator.translate("registration.comfort"));
        int comfort = scanner.nextInt();

        System.out.print(translator.translate("registration.cost"));
        double cost = scanner.nextDouble();

        System.out.print(translator.translate("registration.distance.transport"));
        int distanceTransport = scanner.nextInt();

        System.out.print(translator.translate("registration.distance.shop"));
        int distanceShop = scanner.nextInt();

        System.out.print(translator.translate("registration.distance.university"));
        int distanceUniversity = scanner.nextInt();

        storage.register(new Flat(address, comfort, area, cost, distanceTransport, distanceShop, distanceUniversity));
    }


    /**
     * Method to list all the flats in the list
     */
    private void listFlats() {
        System.out.println(translator.translate("list"));
        for (Flat flat : storage.getFlats()) {
            System.out.println(flat);
        }
    }

    /**
     * Method to find and/or delete the found flat
     */
    private void findAndDeleteFlat() {
        System.out.print(translator.translate("option.choose"));
        String address = scanner.nextLine();

        scanner.nextLine();

        boolean correctInput;
        do {

            System.out.print(translator.translate("option.delete"));
            String input = scanner.nextLine().trim().toLowerCase();
            switch (input) {
                case "y":
                    storage.remove(storage.search(address));
                    System.out.print(translator.translate("message.success"));
                    correctInput = true;
                    break;
                case "n":
                    System.out.print(translator.translate("option.info") + storage.search(address).toString());
                    correctInput = true;
                    break;
                default:
                    System.out.println("Please choose 'y' or 'n'");
                    correctInput = false;
            }

        }while (!correctInput);

    }
}
