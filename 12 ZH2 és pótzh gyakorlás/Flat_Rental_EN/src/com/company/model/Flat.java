package com.company.model;

public class Flat implements Comparable<Flat>  {

    private String address;
    private int comfort;
    private String area;
    private double cost;
    private int distanceTransport;
    private int distanceShop;
    private int distanceUniversity;


    public Flat(String address, int comfort, String area, double cost, int distanceTransport, int distanceShop, int distanceUniversity) {
        this.address = address;
        this.comfort = comfort;
        this.area = area;
        this.cost = cost;
        this.distanceTransport = distanceTransport;
        this.distanceShop = distanceShop;
        this.distanceUniversity = distanceUniversity;
    }


    @Override
    public String toString() {
        return "Flat{" +
                "address='" + address + '\'' +
                ", comfort=" + comfort +
                ", area='" + area + '\'' +
                ", cost=" + cost +
                ", distanceTransport=" + distanceTransport +
                ", distanceShop=" + distanceShop +
                ", distanceUniversity=" + distanceUniversity +
                '}';
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getComfort() {
        return comfort;
    }

    public void setComfort(int comfort) {
        this.comfort = comfort;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getDistanceTransport() {
        return distanceTransport;
    }

    public void setDistanceTransport(int distanceTransport) {
        this.distanceTransport = distanceTransport;
    }

    public int getDistanceShop() {
        return distanceShop;
    }

    public void setDistanceShop(int distanceShop) {
        this.distanceShop = distanceShop;
    }

    public int getDistanceUniversity() {
        return distanceUniversity;
    }

    public void setDistanceUniversity(int distanceUniversity) {
        this.distanceUniversity = distanceUniversity;
    }

    @Override
    public int compareTo(Flat o) {
        return address.compareTo(o.getAddress());
    }
}
