package com.company.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FlatStorage {

    private List<Flat> flats;

    /**
     * Method that returns unmodifiable list to the user.
     * This method provide users with "read-only" access to internal list
     * @return unmodifiable list of flats
     */
    public List<Flat> getFlats() {
        return Collections.unmodifiableList(flats);
    }

    public FlatStorage() {
        this.flats = new ArrayList<>();
    }

    public void register(Flat flat){
        flats.add(flat);
    }

    public void remove(Flat flat){
        flats.remove(flat);
    }

    public void sort(){
        Collections.sort(flats);
    }

    public Flat search(String address){
        for (Flat flat: flats) {
            if(flat.getAddress().equals(address)){
                return flat;
            }
        }

        return null;
    }
}
