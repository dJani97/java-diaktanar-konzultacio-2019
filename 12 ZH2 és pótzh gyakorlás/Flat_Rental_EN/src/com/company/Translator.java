package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static com.company.utils.BuildConfig.CHARSET;
import static com.company.utils.BuildConfig.FILE_PATH_ENGLISH;

/*
  Singleton Translator class which main function is to create a Dictionary
*/
public class Translator {

    private static Translator sharedInstance;

    private Map<String, String> dictionary = new HashMap<>();

    private Translator() {
    }

    public static Translator getSharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new Translator();
        }
        return sharedInstance;
    }

    /**
     * Method to translate the chosen key-value pair
     * @param input key
     * @return the value to be translated
     */
    public String translate(String input) {
        return dictionary.get(input);
    }

    /**
     * Method that takes user input which corresponds to the chosen language.
     * Currently only one language is available - English.
     * @param input user scanned input
     * @throws Exception if user enters wrong value
     */
    public void setLanguage(int input) throws Exception {
        switch (input) {
            case 1:
                readFile(FILE_PATH_ENGLISH);
                break;
            default:
                throw new Exception("Invalid input!");
        }
    }

    /**
     * Method to the read the file
     * @param filePath is chosen based on the user chosen language.
     * Currently only one language is available - English.
     * @throws FileNotFoundException if the file does not exist or the path is wrong
     */
    private void readFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner scanner = new Scanner(file, CHARSET);

        while (scanner.hasNextLine()) {

            String line = scanner.nextLine();

            // Read only if the line is empty
            if (!line.isEmpty()) {

                // Split line in only in two strings.
                // One String is key, second is an actual value
                String data[] = line.split("-", 2);

                String key = data[0].trim();
                String value = data[1].trim();

                if (data.length == 2) {
                    dictionary.put(key, value); // Add key value pairs to the dictionary
                }
            }
        }

        // Close scanner after reading a file
        scanner.close();
    }
}


