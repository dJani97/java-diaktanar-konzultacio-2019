package com.company.utils;

/*
  Helper class to keep all the constant values.
 */
public class BuildConfig {

    public static final String CHARSET = "UTF-8";

    private static final String BASE_FILE_PATH = "src/data/";

    public static final String FILE_PATH_ENGLISH = BASE_FILE_PATH + "dictionary-english.txt";

}
