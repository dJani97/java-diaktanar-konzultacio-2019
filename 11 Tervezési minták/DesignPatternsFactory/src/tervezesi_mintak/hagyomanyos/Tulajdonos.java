package tervezesi_mintak.hagyomanyos;

import megosztott.Auto;
import megosztott.Gumi;
import megosztott.Motor;

public class Tulajdonos {
    Auto auto;

    public void autotVasarol() {

        Motor motor = new Motor("benzin");
        Gumi gumi = new Gumi("continental");

        this.auto = new Auto(motor, gumi, 2019);
    }
}
