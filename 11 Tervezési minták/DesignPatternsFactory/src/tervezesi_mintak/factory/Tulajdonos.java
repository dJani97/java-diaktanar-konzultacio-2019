package tervezesi_mintak.factory;

import megosztott.Auto;
import megosztott.Gumi;
import megosztott.Motor;

public class Tulajdonos {
    Auto auto;

    public void autotVasarol() {
        this.auto = AutoGyar.autoLetrehozasa("diesel", "michelin");
    }
}
