package tervezesi_mintak.factory;

import megosztott.Auto;
import megosztott.Gumi;
import megosztott.Motor;

import java.util.Calendar;

/**
 * A konvenció az lenne, hogy "AutoFactory"!
 */
public class AutoGyar {

    public static Auto autoLetrehozasa(String motorTipusa, String gumiTipusa) {
        Motor motor = new Motor(motorTipusa);
        Gumi gumi = new Gumi(gumiTipusa);
        return new Auto(motor, gumi, 2019);
    }
}
