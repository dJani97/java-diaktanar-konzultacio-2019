package megosztott;

public class Gumi {
    private String tipus;

    public Gumi(String tipus) {
        this.tipus = tipus;
    }

    @Override
    public String toString() {
        return "Gumi{" +
                "tipus='" + tipus + '\'' +
                '}';
    }

}
