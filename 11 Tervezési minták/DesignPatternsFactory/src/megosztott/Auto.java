package megosztott;

public class Auto {
    private Motor motor;
    private Gumi gumi;
    private int gyartasiEv;

    public Auto(Motor motor, Gumi gumi, int gyartasiEv) {
        this.gyartasiEv = gyartasiEv;
        this.motor = motor;
        this.gumi = gumi;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "motor=" + motor +
                ", gumi=" + gumi +
                '}';
    }
}
