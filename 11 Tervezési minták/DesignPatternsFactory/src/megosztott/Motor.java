package megosztott;

public class Motor {
    private String tipus;

    public Motor(String tipus) {
        this.tipus = tipus;
    }

    @Override
    public String toString() {
        return "Motor{" +
                "tipus='" + tipus + '\'' +
                '}';
    }
}
