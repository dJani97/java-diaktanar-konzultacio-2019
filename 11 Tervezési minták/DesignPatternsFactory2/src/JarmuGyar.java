import megosztott.Auto;
import megosztott.Bicikli;
import megosztott.Jarmu;
import megosztott.Tricikli;

/**
 * A konvenció az lenne, hogy "JarmuFactory"!
 */
public class JarmuGyar {

    public static Jarmu jarmuLetrehozasa(int kerekekSzama) {
        switch (kerekekSzama) {
            case 2:
                return new Bicikli();
            case 3:
                return new Tricikli();
            case 4:
                return new Auto();
            default:
                return null;
        }
    }

}


