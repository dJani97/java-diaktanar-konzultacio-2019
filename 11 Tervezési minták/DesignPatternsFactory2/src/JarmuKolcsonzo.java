import megosztott.*;

public class JarmuKolcsonzo {

    public static Jarmu jarmuKolcsonzese(int utasokSzama) {

        if (utasokSzama == 1)
            return new Bicikli();

        else if (utasokSzama == 2)
            return new Tricikli();

        else if (utasokSzama <= 5)
            return new Auto();

        else if (utasokSzama <= 15)
            return new Kisbusz();

        else if (utasokSzama <= 60)
            return new Busz();

        else
            return null;
    }
}
