public class SingletonLogger {

    private static SingletonLogger logger;

    public static int szamlalo = 0;

    private SingletonLogger() {
        szamlalo++;
    }

    public static SingletonLogger getInstance() {
        if (logger == null) {
            logger = new SingletonLogger();
        }
        return logger;
    }

    public void log(String uzenet) {
        System.out.println("SingletonLogger " + uzenet);
    }

}
