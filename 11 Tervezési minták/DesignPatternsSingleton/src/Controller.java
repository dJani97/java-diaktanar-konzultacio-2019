public class Controller {



    public void start() {
        HagyomanyosLogger logger = new HagyomanyosLogger();
        logger.log("Hello");

        SingletonLogger.getInstance().log("Üzenet a start()-ból");

        csinalValamit();
        EgyMasikOsztaly.metodus();

        System.out.println("Hagyományos loggerek: " + HagyomanyosLogger.szamlalo);
        System.out.println("Singleton loggerek: " + SingletonLogger.szamlalo);
    }

    public void csinalValamit() {
        HagyomanyosLogger logger = new HagyomanyosLogger();
        logger.log("Hello 2");

        SingletonLogger.getInstance().log("Üzenet a csinalValamit()-ból");

    }
}
