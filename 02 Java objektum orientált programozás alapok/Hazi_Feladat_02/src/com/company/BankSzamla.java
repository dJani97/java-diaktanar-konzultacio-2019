package com.company;

public class BankSzamla {
    private static int LEKERDEZES_KOLTSEGE = 50;
    private static int PENZFELVETEL_KOLTSEGE = 100;

    private int egyenleg;
    private String tulajdonosNeve;

    public BankSzamla(String tulajdonosNeve) {
        this.tulajdonosNeve = tulajdonosNeve;
        this.egyenleg = 0;
    }

    public int egyenlegLekerdezes() {
        this.egyenleg -= LEKERDEZES_KOLTSEGE;
        return this.egyenleg;
    }

    public void penzBefizetes(int osszeg) {
        this.egyenleg += osszeg;
    }

    public int penzFelvetel(int felvenniKivantOsszeg) {
        if (this.egyenleg >= felvenniKivantOsszeg+PENZFELVETEL_KOLTSEGE) {
            this.egyenleg -= PENZFELVETEL_KOLTSEGE;
            this.egyenleg -= felvenniKivantOsszeg;
            return felvenniKivantOsszeg;
        } else {
            System.out.println("Nincs elegendő pénz a számlán!");
            return 0;
        }
    }
}
