package com.company;

public class Controller {
    public void start() {

        BankSzamla bellaSzamlaja = new BankSzamla("Bella");
        BankSzamla belaSzamlaja = new BankSzamla("Béla");

        bellaSzamlaja.penzBefizetes(1500);
        bellaSzamlaja.penzFelvetel(500);

        belaSzamlaja.penzBefizetes(1000);
        belaSzamlaja.penzFelvetel(100);
        belaSzamlaja.penzFelvetel(1000);

        System.out.println("Egyenleg Bella számláján: " + bellaSzamlaja.egyenlegLekerdezes());
        System.out.println("Egyenleg Béla számláján: "  + belaSzamlaja.egyenlegLekerdezes());
    }
}
