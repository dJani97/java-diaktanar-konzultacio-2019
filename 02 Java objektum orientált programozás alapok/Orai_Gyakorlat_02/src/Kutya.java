public class Kutya {

    public String nev;
    public int cukisag;

    /**
     * Konstruktor:
     */
    public Kutya(String kutyaNeve, int kutyaCukisaga) {
        this.nev = kutyaNeve;
        this.cukisag = kutyaCukisaga;
    }

    @Override
    public String toString() {
        return "Kutya{" +
                "nev='" + nev + '\'' +
                ", cukisag=" + cukisag +
                '}';
    }
}
