public class Controller {

    public void start() {

        Kutya kuty = new Kutya("Béla", 15);
        Kutya kuty2 = new Kutya("Balázs", 3);
        Kutya kuty3 = new Kutya("Bella", 15);
        Kutya kuty4 = new Kutya("Bubu", 10);

        KutyaMenhely menhely = new KutyaMenhely();
        menhely.kutyaBefogadasa(kuty);
        menhely.kutyaBefogadasa(kuty2);
        menhely.kutyaBefogadasa(kuty3);
        menhely.kutyaBefogadasa(kuty4);


        Kutya keresettKutya = menhely.kutyaKeresese("Balázs");
        System.out.println("A keresés által talált kutya:" + keresettKutya);


        Kutya gyoztes = menhely.cukisagVerseny();
        System.out.println("\nA nem-fair verseny győztese: " + gyoztes);

        System.out.println("\nA fair verseny győztesei:");
        menhely.fairCukisagVerseny();


        /*
         * Összes kutya lekérése és kiírása for-ciklus segítéségel:
         */
        System.out.println("\nA menhelyen lévő összes kutya:");
        Kutya[] kutyak = menhely.kutyakLekerese();
        for (int i = 0; i < kutyak.length; i++) {
            if(kutyak[i] == null) {
                // erre a break-re azért van szükség, mert különben a tömb üres helyein lévő tövábbi 97 darab "null" értékű kutyát is kiírná a program
                break;
            }
            System.out.println("Kutya"+i + ":" + kutyak[i]);
        }
    }
}
