public class KutyaMenhely {

    public static int maximumKutyakSzama = 101;

    private Kutya[] kutyak = new Kutya[maximumKutyakSzama];
    private int kutyakSzama = 0;

    // Konstruktor jelenleg nincs, tehát default konstruktort használunk

    public void kutyaBefogadasa(Kutya ujKutya) {
        this.kutyak[kutyakSzama] = ujKutya;
        this.kutyakSzama++;
    }

    public Kutya kutyaKeresese(String keresettNev) {
        Kutya eredmeny = null;

        for (int i = 0; i < kutyakSzama; i++) {
            Kutya jelenlegiKutya = kutyak[i];
            if (jelenlegiKutya.nev.equals(keresettNev)) {
                eredmeny = jelenlegiKutya;
            }
        }

        return eredmeny;
    }

    public Kutya[] kutyakLekerese() {
        return this.kutyak;
    }

    public Kutya cukisagVerseny() {
        Kutya eredmeny = null;
        int eddigiLegnagyobb = 0;

        for (int i = 0; i < kutyakSzama; i++) {
            Kutya jelenlegiKutya = kutyak[i];

            if (jelenlegiKutya.cukisag > eddigiLegnagyobb) {
                eredmeny = jelenlegiKutya;
                eddigiLegnagyobb = jelenlegiKutya.cukisag;
            }
        }

        return eredmeny;
    }

    public void fairCukisagVerseny() {
        int eddigiLegnagyobb = 0;

        for (int i = 0; i < kutyakSzama; i++) {
            Kutya jelenlegiKutya = kutyak[i];

            if (jelenlegiKutya.cukisag > eddigiLegnagyobb) {
                eddigiLegnagyobb = jelenlegiKutya.cukisag;
            }
        }

        for (int i = 0; i < kutyakSzama; i++) {
            Kutya jelenlegiKutya = kutyak[i];

            if (jelenlegiKutya.cukisag == eddigiLegnagyobb) {
                System.out.println(jelenlegiKutya);
            }
        }
    }



}
