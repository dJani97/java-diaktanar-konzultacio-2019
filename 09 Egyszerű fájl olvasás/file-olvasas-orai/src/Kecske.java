public class Kecske {
    private String nev;
    private int eletkor;
    private boolean kapottOltast;

    public Kecske(String nev, int eletkor, boolean kapottOltast) {
        this.nev = nev;
        this.eletkor = eletkor;
        this.kapottOltast = kapottOltast;
    }

    @Override
    public String toString() {
        return "Kecske{" +
                "nev='" + nev + '\'' +
                ", eletkor=" + eletkor +
                ", kapottOltast=" + kapottOltast +
                '}';
    }
}
