import java.io.InputStream;
import java.util.*;

public class Controller {

    List<Kecske> kecskeLista = new ArrayList<>();

    public void start() {
        fajlBeolvasas();
        allatokListazasa();
    }

    private void allatokListazasa() {
        for (Object k : kecskeLista) {
            System.out.println(k);
        }
    }

    private void fajlBeolvasas() {
        InputStream inputStream = getClass().getResourceAsStream("/adatok/kecskek.txt");
        Scanner scanner = new Scanner(inputStream, "UTF-8");

        while (scanner.hasNextLine()) {
            String sor = scanner.nextLine();

            if (!sor.isEmpty()) {
                String[] sorAdatai = sor.split(";");
                String nev = sorAdatai[0];
                int eletkor = Integer.parseInt(sorAdatai[1]);
                boolean oltastKapott = Boolean.parseBoolean(sorAdatai[2]);
                Kecske kecske = new Kecske(nev, eletkor, oltastKapott);
                kecskeLista.add(kecske);
            }
        }
    }
}
