public class Konyv {

    // STATIKUS ADATOK

    private static int afa = 27;

    // NEM STATIKUS ADATOK, privátként

    private String cim;
    private String szerzo;
    private int oldalSzam;
    private int beszerzesiAr;

    // KONSTRUKTOR(ok)

    public Konyv(String cim, String szerzo, int oldalSzam, int beszerzesiAr) {
        this.cim = cim;
        this.szerzo = szerzo;
        this.oldalSzam = oldalSzam;
        this.beszerzesiAr = beszerzesiAr;
    }

    // constructor overloading
    public Konyv(String cim, String szerzo) {
        this.cim = cim;
        this.szerzo = szerzo;
    }

    public Konyv(Konyv lemasolandoKonyv) {
        this.cim = lemasolandoKonyv.getCim();
        this.szerzo = lemasolandoKonyv.getSzerzo();
        this.oldalSzam = lemasolandoKonyv.getOldalSzam();
        this.beszerzesiAr = lemasolandoKonyv.getBeszerzesiAr();
    }



    // AFA -> adó
    // adó,beszerzesiAr -> eladasiAr()


    // METODUSOK

    public double ado() {
        double tortAfa = afa / 100.0;
        return beszerzesiAr * tortAfa;
    }

    public int eladasiAr() {
        return (int) (beszerzesiAr + ado());
    }

    @Override
    public String toString() {
        return "Konyv{" +
                "cim='" + cim + '\'' +
                ", szerzo='" + szerzo + '\'' +
                ", oldalSzam=" + oldalSzam +
                ", eladasiAr=" + eladasiAr() +
                '}';
    }

    // GET-SET METÓDUSOK

    public static int getAfa() {
        return afa;
    }

    public static void setAfa(int afa) {
        Konyv.afa = afa;
    }

    public String getCim() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public String getSzerzo() {
        return szerzo;
    }

    public void setSzerzo(String szerzo) {
        this.szerzo = szerzo;
    }

    public int getOldalSzam() {
        return oldalSzam;
    }

    public void setOldalSzam(int oldalSzam) {
        this.oldalSzam = oldalSzam;
    }

    public int getBeszerzesiAr() {
        return beszerzesiAr;
    }

    public void setBeszerzesiAr(int beszerzesiAr) {
        this.beszerzesiAr = beszerzesiAr;
    }
}
