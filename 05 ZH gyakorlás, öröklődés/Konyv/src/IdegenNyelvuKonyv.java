public class IdegenNyelvuKonyv extends Konyv {

    static private int minimalisAr = 500;

    private int nehezseg;

    public IdegenNyelvuKonyv(String cim, String szerzo, int oldalSzam,
                             int beszerzesiAr, int nehezseg) {

        super(cim, szerzo, oldalSzam, beszerzesiAr);
        this.nehezseg = nehezseg;
    }

    @Override
    public String toString() {
        return "IdegenNyelvu"
                + super.toString()
                + " nehezseg="+nehezsegMegnevese()
                + " szokincs="+szokincs();
    }

    private int szokincs() {
        int szavakSzama = 0;
        switch (nehezseg) {
            case 1:
                szavakSzama = 400;
                break;
            case 2:
                szavakSzama = 1000;
                break;
            case 3:
                szavakSzama = 1500;
                break;
        }
        return szavakSzama;
    }

    private String nehezsegMegnevese() {
        String nehezsegMegnevezese;

        switch (nehezseg) {
            case 1:
                nehezsegMegnevezese = "Könnyű";
                break;
            case 2:
                nehezsegMegnevezese = "Közepes";
                break;
            case 3:
                nehezsegMegnevezese = "Nehez";
                break;
            default:
                nehezsegMegnevezese = "Ismeretlen";
        }
        return nehezsegMegnevezese;
    }

    @Override
    public int eladasiAr() {
        int lehetsegesEladasiAr = super.eladasiAr() - (szokincs() * 5);
        if (lehetsegesEladasiAr < minimalisAr) {
            return minimalisAr;
        }

        return  lehetsegesEladasiAr;
    }
}
