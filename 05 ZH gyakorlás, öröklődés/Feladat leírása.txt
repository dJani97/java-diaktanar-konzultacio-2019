Teljes le�r�s:
/egy�b/Achs Tan�rn� C# gyakorlati feladatai/Gyakorlat_4.pdf

5. feladat
Egy k�nyvet jellemez a c�me, szerz�je, oldalsz�ma, beszerz�si �ra, ad�ja �s elad�si �ra. Az ad�
�rt�k�t az egys�ges �FA-kulcs hat�rozza meg, az elad�si �r a beszerz�si �r �f�val n�velt
�rt�ke. A k�nyv �bor�t�j�ra� (ToString()) a szerz�t, c�met, oldalsz�mot �s elad�si �rat kell
felt�ntetni.
Az idegen nyelv� k�nyvet szint�n jellemzik ezek az adatok, valamint egy, a k�nyv
neh�zs�g�re utal� jelz�. Ezt a k�nyvp�ld�ny l�trehoz�sakor egy eg�sz sz�mmal jelezz�k, de a
k�nyv �bor�t�j�ra� a megfelel� sz� ker�l majd, �s az is, hogy a k�nyv olvas�sa h�ny idegen sz�
ismeret�t ig�nyli (mekkora sz�kincs kell hozz�). Alap�rtelmezett adatok lehetnek p�ld�ul ezek:
1-es szint: 400 sz�, alapfok; 2-es szint: 1000 sz�, k�z�pfok, 3-as szint: 1500 sz� fels�fok. Hogy
serkents�k a nyelvtanul�st, a k�nyv �r�b�l elengedik a sz�kincs ezrel�k�nek megfelel�
�sszeget, de egy minim�lis �rn�l olcs�bban semmit sem adnak.
a/ Tesztelje az elk�sz�lt oszt�lyokat n�h�ny p�ld�ny l�trehoz�s�val �s ki�rat�s�val.
Pr�b�lja megoldani azt is, hogy m�dos�tani lehessen az idegen nyelv� k�nyvek neh�zs�g�re
vonatkoz� sz�-sz�mot, feliratot. Ha el�g �gyes, avval is pr�b�lkozhat, hogy a szintek sz�ma is
lehessen elt�r�.
b/ Olvasson be n�h�ny adatot, ezekb�l hozzon l�tre k�nyv p�ld�nyokat �s sz�molja ki, hogy
mekkora a bev�tel, ha eladj�k az �sszes k�nyvet. Azt, hogy az �pp aktu�lis k�nyv magyar vagy
idegen nyelv�, egy v�letlen sz�m alapj�n d�ntse el, �s persze, ennek megfelel�en k�rje vagy ne
k�rje be a k�nyv szintj�t.
c/ A mell�kelt konyvek.txt adatf�jlb�l (vagy egy �n �ltal k�sz�tett hasonl� adatf�jlb�l) olvassa
be az adatokat, �s hozza l�tre a k�nyvek list�j�t, majd sz�molja ki, mekkora az �ssz-bev�tel, ha
eladjuk �ket. Melyik(ek) a legdr�g�bb �s melyik(ek) a legolcs�bb k�nyv(ek)?