import java.util.ArrayList;
import java.util.List;

public class Controller {
    public void start() {

        arraysDemo();
        listsDemo();

    }

    void listsDemo() {
        List<String> strings = new ArrayList<>();
        strings.add("one");
        strings.add("two");
        strings.add("three");
        strings.add("four");

        // így is működik a Print:
        System.out.println(strings);

        strings.remove("three");

        System.out.println(strings);
    }

    void arraysDemo() {
        String[] strings = new String[4];
        strings[0] = "one";
        strings[1] = "two";
        strings[2] = "three";
        strings[3] = "four";

        printArray(strings);

        // törlés:
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals("three")) {
                strings[i] = null;
            }
        }

        printArray(strings);
    }

    void printArray(Object[] array) {
        System.out.println("\nPrinting array:");
        for (int i = 0; i <array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
