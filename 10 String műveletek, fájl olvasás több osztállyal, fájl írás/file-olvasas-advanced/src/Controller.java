import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal", "SpellCheckingInspection"})
public class Controller {
    private List<Honap> honapok = new ArrayList<>();
    private String fajlUtvonal = "/adatok/datumok.txt";
    private String charSet = "UTF-8";

    public void start() {
        fajlBeolvasas();
        honapokListazasa();
    }

    private void honapokListazasa() {
        for (Object k : honapok) {
            System.out.println(k);
        }
    }

    private void fajlBeolvasas() {
        InputStream inputStream = getClass().getResourceAsStream(fajlUtvonal);
        Scanner scanner = new Scanner(inputStream, charSet);

        while (scanner.hasNextLine()) {
            String sor = scanner.nextLine();

            if (!sor.trim().isEmpty()) {
                sorFeldolgozasa(sor);
            }
        }
    }

    private void sorFeldolgozasa(String sor) {
        String[] sorAdatai = sor.split("-");

//        for (int i = 0; i < sorAdatai.length; i++) {
//            sorAdatai[i] = sorAdatai[i];
//        }

        String nev = sorAdatai[0].trim();
        int sorszam = Integer.parseInt(sorAdatai[1].trim());
        int napokSzama = Integer.parseInt(sorAdatai[2].replace("days", "").trim());

        Honap honap = new Honap(nev, sorszam, napokSzama);
        honapok.add(honap);
    }
}
