import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal", "SpellCheckingInspection"})
public class ControllerBonyolult {
    private List<Honap> honapok = new ArrayList<>();
    private String fajlUtvonal = "/adatok/datumok_bonyolult.txt";
    private String charSet = "UTF-8";

    public void start() {
        fajlBeolvasas();
        honapokListazasa();
        ujFajlKiirasa();
    }

    private void ujFajlKiirasa() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/adatok/uj_adatfajl.txt"));

            for (Honap h: honapok) {
                writer.write(h.toString());
                writer.newLine();
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            System.out.println("Nem létező útvonal, vagy nincs jogosultság");
            e.printStackTrace();
        }
    }

    private void honapokListazasa() {
        for (Object k : honapok) {
            System.out.println(k);
        }
    }

    private void fajlBeolvasas() {
        InputStream inputStream = getClass().getResourceAsStream(fajlUtvonal);
        Scanner scanner = new Scanner(inputStream, charSet);

        while (scanner.hasNextLine()) {
            String sor = scanner.nextLine();

            if (!sor.isEmpty()) {
                sorFeldolgozasa(sor);
            }
        }
    }

    private void sorFeldolgozasa(String sor) {
        String[] sorAdatai = sor.split("-");

        String nev = sorAdatai[0].trim();
        int sorszam = Integer.parseInt(sorAdatai[1].trim());
        int napokSzama = Integer.parseInt(sorAdatai[2].replace("days", "").trim());

        Honap honap;
        if (sorAdatai.length > 3) {
            String szunetTipus = sorAdatai[3].trim();
            honap = new SzunetHonap(nev, sorszam, napokSzama, szunetTipus);
        } else {
            honap = new Honap(nev, sorszam, napokSzama);
        }

        honapok.add(honap);
    }
}
