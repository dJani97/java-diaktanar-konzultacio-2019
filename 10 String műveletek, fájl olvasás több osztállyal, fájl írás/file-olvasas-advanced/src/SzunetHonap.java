public class SzunetHonap extends Honap {

    private String tipus;

    public SzunetHonap(String megnevezes, int sorszam, int napokSzama, String tipus) {
        super(megnevezes, sorszam, napokSzama);
        this.tipus = tipus;
    }

    @Override
    public String toString() {
        return "Szunet" + super.toString() + " - ez " + this.tipus + "!";
    }
}
