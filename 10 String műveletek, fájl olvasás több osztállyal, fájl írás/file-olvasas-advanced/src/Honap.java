public class Honap {
    private String megnevezes;
    private int sorszam;
    private int napokSzama;

    public Honap(String megnevezes, int sorszam, int napokSzama) {
        this.megnevezes = megnevezes;
        this.sorszam = sorszam;
        this.napokSzama = napokSzama;
    }

    @Override
    public String toString() {
        return "Honap{" +
                "megnevezes=" + megnevezes +
                ", sorszam=" + sorszam +
                ", napokSzama=" + napokSzama +
                '}';
    }
}
