public class IdegenNyelvuKonyv extends Konyv {

    static private int minimalisAr = 500;

    private Nehezseg nehezseg;

    public IdegenNyelvuKonyv(String cim, String szerzo, int oldalSzam,
                             int beszerzesiAr, Nehezseg nehezseg) {

        super(cim, szerzo, oldalSzam, beszerzesiAr);
        this.nehezseg = nehezseg;
    }

    @Override
    public String toString() {
        return "IdegenNyelvu"
                + super.toString()
                + " nehezseg="+nehezseg.getNev()
                + " szokincs="+nehezseg.getIdegenSzavakSzama();
    }

    @Override
    public int eladasiAr() {
        int lehetsegesEladasiAr = super.eladasiAr() - kedvezmenyOsszege();
        if (lehetsegesEladasiAr < minimalisAr) {
            return minimalisAr;
        }

        return  lehetsegesEladasiAr;
    }

    private int kedvezmenyOsszege() {
        return nehezseg.getIdegenSzavakSzama() * 5;
    }
}
