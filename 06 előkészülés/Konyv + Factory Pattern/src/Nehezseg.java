public class Nehezseg {
    private int szint;
    private String nev;
    private int idegenSzavakSzama;

    public int getSzint() {
        return szint;
    }

    public void setSzint(int szint) {
        this.szint = szint;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getIdegenSzavakSzama() {
        return idegenSzavakSzama;
    }

    public void setIdegenSzavakSzama(int idegenSzavakSzama) {
        this.idegenSzavakSzama = idegenSzavakSzama;
    }

    @Override
    public String toString() {
        return "Nehezseg{" +
                "szint=" + szint +
                ", nev='" + nev + '\'' +
                ", idegenSzavakSzama=" + idegenSzavakSzama +
                '}';
    }
}
