import java.util.Arrays;
import java.util.List;

public class NehezsegFactory {

    public static String ismeretlenNehezsegElnevezes = "Ismeretlen";
    public static List<String> nehezsegElnevezesek = Arrays.asList("Könnyű", "Közepes", "Nehéz", "Profi");
    public static List<Integer> nehezsegSzavakSzamai = Arrays.asList(400, 1000, 1500, 4000);


    /**
     *  Gyártósor
     */
    public static Nehezseg getNehezseg(int nehezsegiSzint) {
        Nehezseg legyartottNehezseg = new Nehezseg();

        // a 0-tól való indexelés miatt levonunk 1-et:
        int nehezsegiSzintIndexe = nehezsegiSzint - 1;

        if (0 < nehezsegiSzintIndexe &&
                nehezsegiSzintIndexe < nehezsegElnevezesek.size()) {

            legyartottNehezseg.setSzint(nehezsegiSzint);
            legyartottNehezseg.setNev(nehezsegElnevezesek.get(nehezsegiSzintIndexe));
            legyartottNehezseg.setIdegenSzavakSzama(nehezsegSzavakSzamai.get(nehezsegiSzintIndexe));
        } else {
            legyartottNehezseg.setSzint(nehezsegiSzint);
            legyartottNehezseg.setNev(ismeretlenNehezsegElnevezes);
        }

        return legyartottNehezseg;
    }
}
