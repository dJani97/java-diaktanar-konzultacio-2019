import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Konyv konyv  = new Konyv("Vukk",
                "Fekete Istvány", 250,
                2000);

        System.out.println(konyv.eladasiAr());

        Konyv megNemJelentKonyv = new Konyv("Cim", "Szerzo neve");
        System.out.println("Hírdetés: jövő héttől elérhető: "
                + megNemJelentKonyv.getCim());

        megNemJelentKonyv.setBeszerzesiAr(1000);
        megNemJelentKonyv.setOldalSzam(500);

        Konyv masolat = new Konyv(megNemJelentKonyv);

        System.out.println(masolat.getCim() +" " + masolat.getBeszerzesiAr());

        System.out.println("\n______________ Idegen Nyelvű Könyvek: ______________\n");

        IdegenNyelvuKonyv idegenNyelvu = new IdegenNyelvuKonyv("cím",
                "szerző", 1000,
                5000, NehezsegFactory.getNehezseg(2));

        System.out.println(idegenNyelvu);
        System.out.println(idegenNyelvu.eladasiAr());


        /*
         * Random szám generálás minta:
         */

        for (int i = 0; i < 10; i++) {

            // adatok bekerese
            int veletlenSzam = (int) (2 * Math.random()) + 1;
            System.out.println(veletlenSzam);

            /*
            if (veletlenSzam == 1)
                new Kony(...)

            else
                bekerjük a nehezseget is
                new IdegenNyelvuKonyv(...)

            */
        }


        /*
         * Lista létrehozás minta:
         */

        List<Konyv> konyvek = new ArrayList<>();

        konyvek.add(konyv);
        konyvek.add(idegenNyelvu);
        System.out.println(konyvek);
        konyvek.remove(konyv);
        System.out.println(konyvek);

        /*
         * Lista iteráció minta:
         */

        System.out.println("\n______________ Iteráció Minta: ______________\n");

        konyvek.add(konyv);
        konyvek.add(new Konyv("Konyv cim", "Szerzőőő", 120, 3542));

        for (Konyv jelenlegiKonyv : konyvek) {

            System.out.println("Ciklus változó indexe:  \t" + konyvek.indexOf(jelenlegiKonyv));
            System.out.println("Ciklus változó tartalma:\t" + jelenlegiKonyv);

        }

        /*
         * NehezsegFactory - használat:
         */

        Nehezseg nehezseg = NehezsegFactory.getNehezseg(3);
        IdegenNyelvuKonyv idegenNyelvuKonyv = new IdegenNyelvuKonyv(
                "Life in America",
                "Külföldi Károly",
                750,
                4500,
                nehezseg);

        /*
         * NehezsegFactory - Hibás input kezelésének tesztelése:
         */

        Nehezseg nemLehezoNehezseg1 = NehezsegFactory.getNehezseg(-1);
        Nehezseg nemLehezoNehezseg2 = NehezsegFactory.getNehezseg(99);

        System.out.println(nehezseg);
        System.out.println(nemLehezoNehezseg1);
        System.out.println(nemLehezoNehezseg2);

    }
}
